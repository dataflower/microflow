#! /usr/bin/env python3

# coding: utf-8
#
# test-flow.py - a simple test for flow.py, inspired by an old xscreensaver
#
# Copyright (C) 2014  Pedro Ângelo <pangelo@void.io>
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the
# sale, use or other dealings in this Software without prior written
# authorization.

import random
import microflow as flow

class TextGenerator(flow.Process):
    "Outputs a given string a specified number of times"

    def __init__(self, name):
        super(TextGenerator, self).__init__(name)
        self.optport = self.make_input('OPTIONS')
        self.outport = self.make_output('OUT')
        self.text = "Generated Text"
        self.repeat = -1

    def process(self):
        if not self.optport.empty():
            options = self.receive(self.optport)
            self.text = options[0]
            self.repeat = options[1]
            self.drop(options)

        if self.repeat > 0:
            self.send(self.outport, self.make_packet("", self.text))
            self.repeat = self.repeat - 1        
        else:
            self.stop()


class TextGlitch(flow.Process):
    "randomly modifies the text at the IN port and sends it to the OUT port"

    def __init__(self, name):
        super(TextGlitch, self).__init__(name)
        self.inport = self.make_input('IN')
        self.outport = self.make_output('OUT')

    def process(self):
        text = self.receive(self.inport)

        if text.type == flow.Packet.EOS:
            self.drop(text)
            self.stop()
            return

        glitched = list(text[0])
        for _ in range(random.randrange(1,10)):
            pos = random.randrange(len(glitched))
            glitched[pos] = chr(random.randrange(31,127))

        self.drop(text)
        self.send(self.outport, self.make_packet("", ''.join(glitched)))


class ConsoleWriter(flow.Process):
    "Writes text sent to IN port to the standard output"

    def __init__(self, name):
        super(ConsoleWriter, self).__init__(name)
        self.inport = self.make_input('IN')

    def process(self):
        text = self.receive (self.inport)

        if text.type == flow.Packet.EOS:
            self.drop(text)
            self.stop()
            return

        print (text[0])
        self.drop(text)


flow.QUEUE_SIZE = 1

graph = flow.Graph('main')
graph.add(TextGenerator('gen1'))
graph.add(TextGenerator('gen2'))
graph.add(TextGenerator('gen3'))
graph.add(TextGlitch('glitch1'))
graph.add(ConsoleWriter('display'))

graph.connect('gen1.OUT', 'glitch1.IN')
graph.connect('gen2.OUT', 'glitch1.IN')
graph.connect('gen3.OUT', 'glitch1.IN')
graph.connect('glitch1.OUT', 'display.IN')

text = 'All work and no play makes {} a dull boy'

graph.initial_packet('gen1.OPTIONS', flow.Packet.data('', text.format('Alfredo'), 2))
graph.initial_packet('gen2.OPTIONS', flow.Packet.data('', text.format('Paul'), 2))
graph.initial_packet('gen3.OPTIONS', flow.Packet.data('', text.format('Henri'), 2))

network = flow.Network()
network.add_graph (graph)
network.run()
