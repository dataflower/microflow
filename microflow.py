# coding: utf-8
#
# flow.py - minimal Python3 implementation of the Flow-based
#           Programming Model
#
# Copyright (C) 2014  Pedro Ângelo <pangelo@void.io>
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# 
# Except as contained in this notice, the name(s) of the above copyright
# holders shall not be used in advertising or otherwise to promote the
# sale, use or other dealings in this Software without prior written
# authorization.

import threading
import time
import queue
import re


# module-level configuration
QUEUE_SIZE=5


class Packet(tuple):
    """
    A packet is a single unit of data to be processed

    Packets are responsible for allocating their internal data and
    are owned by the processes that receive them. They must be
    either passed on to another process or explicitly destroyed by
    the owning process
    
    There are three major packet types:
     * Data packets that function like tuples
     * Bracket packets are used to delimit packet groups (called substreams)
     * End-of-Stream packets that signal that a process will not send any more data through the stream
    
    Packets can contain other packets in a tree-like structure,
    called chains.
    
    """

    # Packet types
    DATA = 0
    BRACKET_OPEN = 1
    BRACKET_CLOSE = 2
    EOS = 3

    def __new__(cls, _type, _tag, *args):
        t = tuple.__new__(cls, *args)
        t.type = _type
        t.tag = _tag
        t.chains = {}
        return t

    # factory methods to help build new packets    
    @classmethod
    def data(cls, tag="", *data):
        return cls(Packet.DATA, tag, data)

    @classmethod
    def open_bracket(cls, tag=""):
        return cls(Packet.BRACKET_OPEN, tag)

    @classmethod
    def close_bracket(cls, tag=""):
        return cls(Packet.BRACKET_CLOSE, tag)

    @classmethod
    def eos(cls, tag=""):
        return cls(Packet.EOS, tag)

class Port(object):
    "A port is a generic data connection between processes"
    
    def __init__(self, name):
        self.name = name


class InputPort(Port):
    "An input Port receives data from other processes"

    def __init__(self, name):
        super(InputPort, self).__init__(name)
        self.q = queue.Queue(QUEUE_SIZE)
        self.connections = 0
        
    def queue(self, data):
        self.q.put(data)

    def empty(self):
        return self.q.empty()

    def full(self):
        return self.q.full()

    def receive(self):
        return self.q.get()

    def connect(self, upstream_port):
        self.connections = self.connections + 1

    def disconnect(self, upstream_port):
        self.connections = self.connections - 1
        if not self.connected():
            self.q.put(Packet.eos())

    def connected(self):
        return self.connections > 0

class ArrayInputPort(list):
    
    def __init__(self, name, size):
        port_list = [InputPort("{}[{}]".format(name,i)) for i in range(size)]
        super(ArrayInputPort, self).__init__(port_list)


class OutputPort(Port):
    "An OutputPort sends data to another process"

    def __init__(self, name):
        super(OutputPort, self).__init__(name)
        self.target = None

    def connect(self, target_port):
        if self.target == target_port:
            return
        self.target = target_port
        self.target.connect(self)

    def disconnect(self):
        if self.connected():
            self.target.disconnect(self)
            self.target = None

    def send(self, data):
        if self.connected():        
            self.target.queue(data)

    def connected(self):
        return (self.target is not None)

    def empty(self):
        return target.empty()

    def full(self):
        return target.full()


class ArrayOutputPort(list):
    
    def __init__(self, name, size):
        port_list = [OutputPort("{}[{}]".format(name,i)) for i in range(size)]
        super(ArrayOutputPort, self).__init__(port_list)


class ProxyPort(Port):
    "A ProxyPort forwards any packets it receives to its target"

    def __init__(self, name):
        super(ProxyPort, self).__init__(name)
        self.target = None

    def queue(self, data):
        self.target.queue(data)


class Process(threading.Thread):
    """A process is a self contained unit of computation

    Processes are asynchronous units of computation, responsible for
    managing their own internal state. They are used as a way to
    encapsulate the serial parts of a program and should only
    communicate with the rest of the system via packets sent to their ports.

    Processes are oblivious to whete their ports are connected and can
    choose to block when reading certain ports. Processes can also
    block when writing to a port which is full.

    To implement a process the user must implement the process()
    method, which is called when the process is scheduled to do some
    work, and can also implement the __init__() method to perform any
    needed initialization.

    There are two types of process: continuous (commonly known as loopers) and
    on-demand (commonly known as non-loopers).

    Porcesses own both the packets they create and the packets they
    receive, and are responsible for either disposing of them or
    sending them to another process.

    """ 
    
    # Process states
    IDLE = 0
    RUNNING = 1
    BLOCKED = 2
    STOPPED = 3

    def __init__(self, name):
        super(Process, self).__init__()
        self.name = name
        self.ports = {}
        self.packets = 0
        self.state = Process.IDLE

    def make_input(self, port_name):
        port = InputPort(port_name)
        self.ports[port_name] = port
        return port
        
    def make_output(self, port_name):
        port =  OutputPort(port_name)
        self.ports[port_name] = port
        return port

    def make_proxy(self, port_name):
        port = ProxyPort(port_name)
        self.ports[port_name] = port

    def get_port(self, port_id):
        port_name, *port_index = re.split('\[|\]', port_id)
        port = self.ports.get(port_name)
        if port_index:
              port = port[int(port_index[0])]
        return port

    def send(self, port, packet):
        port.send(packet)
        self.packets = self.packets - 1
        
    def receive(self, port):
        packet = port.receive()
        self.packets = self.packets + 1
        return packet

    def make_packet(self, tag, data):
        packet = Packet.data(tag, data)
        self.packets = self.packets + 1
        return packet

    def make_open_bracket(self, tag=""):
        packet = Packet.open_bracket(tag)
        self.packets = self.packets + 1
        return packet

    def make_close_bracket(self, tag=""):
        packet = Packet.close_bracket(tag)
        self.packets = self.packets + 1
        return packet

    def drop(self, packet):
        del packet
        self.packets = self.packets - 1

    def process(self):
        self.stop()

    def run(self):
        self.state = Process.RUNNING
        while self.state != Process.STOPPED:
            self.process()

    def stop(self):
        self.cleanup()
        for port in self.ports.values():
            if port.connected():
                port.disconnect()
            
        self.state = Process.STOPPED

    def sleep(self, msecs):
        time.sleep(msecs / 1000.0)

    def cleanup(self):
        pass


class Graph(Process):
    """
    A graph manages a set of Process objects and their connections

    A graph can contain other graphs. which can schedule themselves,
    and as such create an hierarchical scheduling tree

    There are many differnt Python modules that allow us to create 
    concurrent execution contexts (native threading, multiprocessing, asyncio).
    All of them provide similar  execution and queue abstractions that we can 
    use to model FBP processes and ports.  The *threading* module used here 
    uses native threads but its performance is limited by Python's Global 
    Interpreter Lock.

    """
    
    def __init__(self, name):
        super(Graph, self).__init__(name)
        self.children = {}

    def add(self, process):
        self.children[process.name] = process

    def get_process(self, name):
        return self.children.get(name)

    def get_all_processes(self):
        return self.children.values()

    def remove_process(self, name):
        p = self.children.get(name)
        if p:
            p.stop()
            del self.children['name']

    def connect(self, src, target):
        src_proc, src_port = self.find(src)
        target_proc, target_port = self.find(target)
        
        if not (src_proc and target_proc):
            return

        if not (src_port and target_port):
            return

        src_port.connect(target_port)

    def initial_packet(self, target, packet):
        proc, port = self.find(target)
        port.queue(packet)

    def find(self, target):
        proc_name, port_name = target.split('.')
        proc = self.get_process(proc_name)
        port = proc.get_port(port_name)
        return (proc, port)

    def process(self):
        pass

    def run(self):
        for p in self.children.values():
            p.start()
        for p in self.children.values():
            p.join()
        self.stop()


class Network(object):
    """
    A Network is a simple abstraction to run a set of graphs
    """

    def __init__(self, workers=4):
        self.graphs = []

    def add_graph(self, graph):
        self.graphs.append(graph)

    def run(self):
        for g in self.graphs:
            g.start()
        for g in self.graphs:
            g.join()
